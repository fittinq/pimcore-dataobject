<?php declare(strict_types=1);

namespace Fittinq\Pimcore\DataObject\DataObject\Folder;

use Pimcore\Model\DataObject\Folder;

class FolderRepository
{
    public function getFolder(string $path): ?Folder
    {
        return Folder::getByPath($path);
    }
}
